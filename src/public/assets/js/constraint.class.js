'use strict';

class Constraint {

    constructor(inputElement, event) {
        this._event = event;
        this._inputElement = inputElement;

        this._setErrorIdentifier(inputElement.attr("name"));
    }

    _setErrorIdentifier(elementName) {
        this._errorIdentifier = "input-error-" + elementName;
    }

    _errorTemplate(errorMessage) {
        return `<div class="input-error" id="${this._errorIdentifier}">${errorMessage}</div>`;
    };

    _notifyErrorMessage(errorMessage) {
        this._event.preventDefault();
        let errorElementId = "#" + this._errorIdentifier;

        if ($(errorElementId).length === 0) {
            this._inputElement.after(this._errorTemplate(errorMessage));
        }
    }

    mustBeBiggerThan(length, errorMessage) {
        if (this._inputElement.val().length < length) {
            this._notifyErrorMessage(errorMessage);
        }
    }
}