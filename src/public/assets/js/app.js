'use strict';

$(function () {
    $("#form-create-article").submit(function (event) {

        new Constraint($("[name=title]", this), event)
            .mustBeBiggerThan(3, "Title cannot be empty");

        new Constraint($("[name=content]", this), event)
            .mustBeBiggerThan(3, "Content cannot be empty");

    });
});