<?php

define('APPLICATION_DEBUG', true);
define('DS', DIRECTORY_SEPARATOR);

define('ROOT_PATH', dirname($_SERVER['DOCUMENT_ROOT']));

define('SOURCE_PATH', ROOT_PATH . DS . 'src');
define('LOGS_PATH', ROOT_PATH . DS . 'logs');


define('PDO_DSN','mysql:host=db;dbname=scanditest');
define('PDO_USERNAME','root');
define('PDO_PASSWORD','root');
