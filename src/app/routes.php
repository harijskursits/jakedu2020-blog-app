<?php
require_once 'Model/Domain/Article.php';
require_once 'Model/Domain/User.php';
require_once 'Model/Mapper/ArticleInDatabase.php';
require_once 'Model/Mapper/UserInDatabase.php';
require_once 'Model/Mapper/UserInSession.php';

$userIsAuthorized = !empty((new UserInSession)->fetch());

if($userIsAuthorized) {
    switch ($page) {
        case '/blog/create':
            require_once("template/create.php");
        break;

        case @"/blog/edit?id={$_GET['id']}":
            $article = new Article;
            $article->setId($_GET['id']);

            $articleInDatabase = new ArticleInDatabase($pdo);
            $articleInDatabase->fetch($article);

            require_once("template/edit.php");
        break;

        case '/blog/store':
            $article = new Article;
            $article->setTitle($_POST['title']);
            $article->setContent($_POST['content']);
            $article->setAuthor($_POST['author']);
            $article->setCreatedAt(date("Y-m-d H:i:s"));
            $article->setAsNew();

            $articleInDatabase = new ArticleInDatabase($pdo);
            $articleInDatabase->save($article);

            if($article->isCreated()){
                $response->setMessage('article_store_success',"Article created successfully");
            }else{
                $response->setMessage('article_store_failed',"Article can't be created");
            }

            $response->redirect('/blog');
        break;

        case "/blog/update":
            $article = new Article;
            $article->setId($_POST['id']);
            $article->setTitle($_POST['title']);
            $article->setContent($_POST['content']);
            $article->setAuthor($_POST['author']);
            $article->setCreatedAt(date("Y-m-d H:i:s"));

            $articleInDatabase = new ArticleInDatabase($pdo);
            $articleInDatabase->save($article);

            if($article->isUpdated()){
                $response->setMessage('article_save_success',"Article updated successfully");
            }else{
                $response->setMessage('article_save_failed',"Article can't be updated");
            }

            $response->redirect("/blog/edit?id={$article->id()}");
        break;

        case @"/blog/delete?id={$_GET['id']}":
            $article = new Article;
            $article->setId($_GET['id']);

            $articleInDatabase = new ArticleInDatabase($pdo);
            $articleInDatabase->delete($article);

            if($article->isDeleted()){
                $response->setMessage('article_create_success',"Article deleted successfully");
            }else{
                $response->setMessage('article_create_failed',"Article can't be deleted");
            }

            $response->redirect('/blog');
        break;

        case '/user/logout':
            $userInSession = new UserInSession;
            $userInSession->delete();

            $response->redirect('/blog');
        break;
    }

}

switch ($page) {

    case '/':
        header('location:/blog');
    break;

    case '/blog':
        $articleInDatabase = new ArticleInDatabase($pdo);
        $articleCollection = $articleInDatabase->fetch(new Article);

        require_once("template/blog.php");
    break;


    case '/user/sign-in':
        require_once 'template/sign-in.php';
    break;

    case '/user/authorize':
        $user = User::authorize($_POST['email'], $_POST['password']);

        $userInDatabase = new UserInDatabase($pdo);
        $userInDatabase->fetch($user);

        if ($user->isAuthorized()) {
            $userInSession = new UserInSession;
            $userInSession->save($user);

            $response->setMessage('user_authorization_success', "Signed up successfully!");
            $response->redirect('/blog');
        } else {
            $response->setMessage('user_authorization_failed', "Sign in failed! Invalid credentials.");
            $response->redirect('/user/sign-in');
        }
    break;

    case '/user/sign-up':
        $user = new User;
        $user->setEmail('harijs.kursits@scandiweb.com');
        $user->setPassword('scandiweb1234');

        var_dump($user);
        var_dump($user->password());
    break;

    default:
        require_once("template/404.php");
}