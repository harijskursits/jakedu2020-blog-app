<?php

class User
{
    private $id;
    private $email;
    private $password;
    private $isAuthorized = false;

    public static function authorize($email, $password){
        $user = new self;
        $user->setEmail($email);
        $user->setPassword($password);

        return $user;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function id(): string
    {
        return $this->id;
    }


    public function setEmail(string $email)
    {
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            throw new Exception("Invalid email");
        }

        $this->email = $email;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function setPassword($password)
    {
        if(strlen($password) < 5){
            throw new Exception("Invalid password");
        }

        $this->password = $password;
    }

    public function password(): string
    {
        return hash('sha256', $this->password);
    }

    public function setAsAuthorized()
    {
        $this->isAuthorized = true;
    }

    public function isAuthorized(): bool
    {
        return $this->isAuthorized;
    }
}