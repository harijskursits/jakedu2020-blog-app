<?php

class Article
{
    private $id;
    private $title;
    private $content;
    private $author;
    private $createdAt;
    private $isNew = false;
    private $isUpdated = false;
    private $isDeleted = false;
    private $isCreated = false;

    public function setId(string $id)
    {
        if (strlen($id) < 0) {
            throw new Exception("Invalid id");
        }

        $this->id = $id;
    }

    public function id()
    {
        return $this->id;
    }

    public function hasId()
    {
        return !empty($this->id);
    }

    public function setTitle(string $title)
    {
        if (strlen($title) < 3) {
            throw new Exception("Invalid title");
        }

        $this->title = $title;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function setContent(string $content)
    {
        if (strlen($content) < 3) {
            throw new Exception("Invalid content");
        }

        $this->content = $content;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function setAuthor(string $author)
    {
        if (strlen($author) < 3 || !preg_match('#^[a-z A-Z]+$#', $author)) {
            throw new Exception("Invalid author");
        }

        $this->author = $author;
    }

    public function author(): string
    {
        return $this->author;
    }

    public function setCreatedAt(string $date)
    {
        $this->createdAt = $date;
    }

    public function createdAt(): string
    {
        return $this->createdAt;
    }

    public function createdAtForHumans(): string
    {
        return date("d/M/Y", strtotime($this->createdAt));
    }

    public function setAsNew()
    {
        return $this->isNew = true;
    }

    public function isNew()
    {
        return $this->isNew;
    }

    public function setAsUpdated()
    {
        $this->isUpdated = true;
    }

    public function isUpdated()
    {
        return $this->isUpdated;
    }

    public function setAsDeleted()
    {
        $this->isDeleted = true;
    }

    public function isDeleted()
    {
        return $this->isDeleted;
    }

    public function setAsCreated()
    {
        $this->isCreated = true;
    }

    public function isCreated()
    {
        return $this->isCreated;
    }
}