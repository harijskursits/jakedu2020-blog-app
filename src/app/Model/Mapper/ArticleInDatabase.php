<?php

class ArticleInDatabase
{
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function save(Article $article)
    {
        try{
            if ($article->isNew()) {
                $this->insert($article);
                $article->setAsCreated();
            } else {
                $this->updateById($article);
                $article->setAsUpdated();
            }
        } catch (Exception $exception) {
            throw new Exception("Article can't be saved in database", 0, $exception);
        }
    }

    public function delete(Article $article)
    {
        try{
            if ($article->hasId()) {
                $this->deleteById($article);
            }else{
                $this->deleteAll();
            }
            $article->setAsDeleted();
        } catch (Exception $exception) {
            throw new Exception("Article can't be deleted from database", 0, $exception);
        }
    }

    public function fetch(Article $article)
    {
        try{
            if ($article->hasId()) {
                return $this->fetchById($article);
            } else {
                return $this->fetchAll($article);
            }
        } catch (Exception $exception) {
            throw new Exception("Articles can't be read from database", 0, $exception);
        }
    }

    private function insert(Article $article)
    {
        $query = $this->pdo->prepare("INSERT INTO articles (title, content, author, created_at)VALUES(:title, :content, :author, :createdAt)");
        $query->execute([
            ':title' => $article->title(),
            ':content' => $article->content(),
            ':author' => $article->author(),
            ':createdAt' => $article->createdAt()
        ]);
    }

    private function updateById(Article $article)
    {
        $query = $this->pdo->prepare("UPDATE articles SET title=:title, content=:content, author=:author WHERE id=:id");
        $query->execute([
            ':id' => $article->id(),
            ':title' => $article->title(),
            ':content' => $article->content(),
            ':author' => $article->author()
        ]);
    }

    private function deleteAll(Article $article)
    {
        $query = $this->pdo->prepare("DELETE FROM articles");
        $query->execute();
    }

    private function deleteById(Article $article)
    {
        $query = $this->pdo->prepare("DELETE FROM articles WHERE id=:id");
        $query->execute([':id' => $article->id()]);
    }

    private function fetchAll(Article $article): array
    {
        $query = $this->pdo->prepare("SELECT * FROM articles");
        $query->execute();
        $data = $query->fetchAll();

        $articleCollection = [];

        foreach ($data as $item) {
            $article = clone $article;
            $article->setId($item['id']);
            $article->setTitle($item['title']);
            $article->setContent($item['content']);
            $article->setAuthor($item['author']);
            $article->setCreatedAt($item['created_at']);

            $articleCollection[] = $article;
        }

        return $articleCollection;
    }

    private function fetchById(Article $article): Article
    {
        $query = $this->pdo->prepare("SELECT * FROM articles WHERE id=:id");
        $query->execute([':id' => $article->id()]);
        $item = $query->fetch();

        $article->setTitle($item['title']);
        $article->setContent($item['content']);
        $article->setAuthor($item['author']);
        $article->setCreatedAt($item['created_at']);

        return $article;
    }

}