<?php

class UserInDatabase
{
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function fetch(User $user)
    {
        try{
            return $this->fetchByEmailAndPassword($user);
        } catch (Exception $exception) {
            throw new Exception("User can't be read from database", 0, $exception);
        }
    }

    private function fetchByEmailAndPassword(User $user): User
    {
        $query = $this->pdo->prepare("SELECT * FROM users WHERE email=:email && password=:password");
        $query->execute([':email' => $user->email(), ':password' => $user->password()]);
        $item = $query->fetch(PDO::FETCH_ASSOC);

        if(!empty($item)) {
            $user->setId($item['id']);
            $user->setAsAuthorized();
        }

        return $user;
    }
}