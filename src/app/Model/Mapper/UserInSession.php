<?php

class UserInSession
{
    const USER_SESSION = 'authorized_user';

    public function save(User $user)
    {
        $_SESSION[self::USER_SESSION] = $user->id();
    }

    public function fetch()
    {
        return isset($_SESSION[self::USER_SESSION]) ? $_SESSION[self::USER_SESSION] : null;
    }

    public function delete()
    {
        if(isset($_SESSION[self::USER_SESSION])) unset($_SESSION[self::USER_SESSION]);
    }


}