<meta charset="UTF-8">
<meta name="description" content="Blogging about stuff">
<meta name="keywords" content="HTML, CSS, JavaScript">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/assets/css/layout-grid.css">
<link rel="stylesheet" href="/assets/css/stylesheet.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="/assets/js/constraint.class.js"></script>
<script src="/assets/js/app.js"></script>

<title>My blog</title>
