<?php if($userIsAuthorized): ?>
    <nav>
        <ul>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/blog/create">Create article</a></li>
            <li><a href="/user/logout">Logout</a></li>
        </ul>
    </nav>
<?php else: ?>
    <nav>
        <ul>
            <li><a href="/user/sign-up">Don't have an account? Sign up!</a></li>
            <li><a href="/user/sign-in">Sign in</a></li>
        </ul>
    </nav>
<?php endif; ?>
