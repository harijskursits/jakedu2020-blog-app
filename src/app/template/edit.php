<!doctype html>
<html lang="eng">
    <head>
        <?php require_once 'layout/metadata.php' ?>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <?php require_once 'layout/navigation.php' ?>
                </div>
            </div>
        </div>

        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1>Edit article</h1>
                    </div>
                </div>
            </div>
        </header>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <?= $response->message('article_save_success') ?>
                        <?= $response->message('article_save_failed') ?>

                        <form action="/blog/update" method="post" id="form-create-article">
                            <input name="id" type="hidden" value="<?= $article->id() ?>">
                            <div class="form-control">
                                <div><label for="title">Title</label></div>
                                <div><input name="title" type="text" value="<?= $article->title() ?>"></div>
                            </div>

                            <div class="form-control">
                                <div><label for="content">Content</label></div>
                                <div><textarea name="content"><?= $article->content() ?></textarea></div>
                            </div>

                            <div class="form-control">
                                <div><label for="author">Author</label></div>
                                <div><input name="author" type="text" value="<?= $article->author() ?>"></div>
                            </div>

                            <div class="form-control">
                                <div><button type="submit" value="Save">Save</button></div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </section>

        <?php require_once 'layout/footer.php' ?>

    </body>
</html>
