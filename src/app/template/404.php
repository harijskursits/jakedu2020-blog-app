<!doctype html>
<html lang="eng">
    <head>
        <?php require_once 'layout/metadata.php' ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <?php require_once 'layout/navigation.php' ?>
                </div>
            </div>
        </div>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1>Page not found - 404</h1>
                    </div>
                </div>
            </div>
        </header>
        <?php require_once 'layout/footer.php' ?>
    </body>
</html>

