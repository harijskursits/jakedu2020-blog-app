<!doctype html>
<html lang="eng">
    <head>
        <?php require_once 'layout/metadata.php' ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <?php require_once 'layout/navigation.php' ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <?php if($response->hasMessage()): ?>
                        <?= $response->message('user_authorization_success') ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h1>Welcome to my blog</h1>
                    </div>
                </div>
            </div>
        </header>

        <section>
            <div class="container">
                <div class="row">

                    <div class="col-xl-12 col-xs-12">
                        <?php if($response->hasMessage()): ?>
                            <?= $response->message('article_store_success') ?>
                            <?= $response->message('article_store_failed') ?>
                        <?php endif; ?>
                    </div>

                    <?php if(empty($articleCollection)): ?>
                        <div class="col-xl-12 col-xs-12">
                            <div>No articles created yet.</div>
                        </div>
                    <?php endif ?>

                    <?php foreach($articleCollection as $article): ?>
                        <div class="col-xl-6 col-xs-12">
                            <article class="article">

                                <?php if($userIsAuthorized): ?>
                                    <div class="article__toolbar">
                                        <a href="/blog/edit?id=<?= $article->id() ?>">Edit <i class="fa fa-edit"></i></a>
                                        <a href="/blog/delete?id=<?= $article->id() ?>">Delete <i class="fa fa-trash-alt"></i></a>
                                    </div>
                                <?php endif ?>

                                <header class="article__header">
                                    <h2 class="article__title"><?= $article->title() ?></h2>
                                    <p class="article__subtext"><?= $article->content() ?></p>
                                </header>
                                <footer class="article__footer">
                                    <address>By <?= $article->author() ?></address>
                                    on <time datetime="<?= $article->createdAtForHumans() ?>"><?= $article->createdAtForHumans() ?></time>
                                </footer>

                            </article>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </section>

        <?php require_once 'layout/footer.php' ?>
    </body>
</html>
