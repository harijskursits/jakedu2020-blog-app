<!doctype html>
<html lang="eng">
    <head>
        <?php require_once 'layout/metadata.php' ?>
    </head>
    <body>
        <?php require_once 'layout/navigation.php' ?>
        <h1>Something went wrong doing your request. Please try again later.</h1>
        <?php require_once 'layout/footer.php' ?>
    </body>
</html>
