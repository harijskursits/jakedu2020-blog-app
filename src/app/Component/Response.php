<?php

class Response
{
    const RESPONSE_KEY = 'response_message';

    public function setMessage(string $key, $message)
    {
        $_SESSION[self::RESPONSE_KEY][$key] = $message;
    }

    public function message(string $key): ?string
    {
        $message = null;
        if (isset($_SESSION[self::RESPONSE_KEY][$key])) {
            $message = $_SESSION[self::RESPONSE_KEY][$key];
            unset($_SESSION[self::RESPONSE_KEY]);
        }

        return $message;
    }

    public function hasMessage()
    {
        return !empty($_SESSION[self::RESPONSE_KEY]);
    }

    public function redirect(string $location): void
    {
        header("location: {$location}");
    }
}