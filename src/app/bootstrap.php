<?php

require_once 'config.php';
require_once 'Component/Response.php';

ini_set("log_errors", 1);
ini_set('error_log', LOGS_PATH.DS.'php.log');

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

if (APPLICATION_DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

try {
    session_start();

    $page = null;
    if (isset($_SERVER['REQUEST_URI'])) {
        $page = $_SERVER['REQUEST_URI'];
    }

    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_CASE => PDO::CASE_NATURAL,
        PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
    ];

    $pdo = new PDO(PDO_DSN,PDO_USERNAME,PDO_PASSWORD, $options);
    $response = new Response;

    require_once 'routes.php';
} catch (Exception $exception) {

    if (APPLICATION_DEBUG) {
        print("<pre>Exception: {$exception->getMessage()} in {$exception->getFile()}:{$exception->getLine()} <pre>");
        print("<pre>{$exception->getTraceAsString()}<pre>");
        print("<pre>{$exception->getPrevious()}<pre>");
    } else {
        print "<pre>Whoops, looks like something went wrong.</pre>";
    }

    file_put_contents(LOGS_PATH . DS . 'exceptions.log', $exception . "\n\n", FILE_APPEND);
}